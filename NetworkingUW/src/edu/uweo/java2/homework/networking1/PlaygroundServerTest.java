package edu.uweo.java2.homework.networking1;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

import edu.uweo.java2.homework.networking1.PlaygroundServer.PlaygroundClient;

public class PlaygroundServerTest {
	static final AbstractClient[] CLIENTS = {
		new AbstractClient() {
			public void speak() throws IOException {
				String[] enquiries = {
					"time",
					"echo hello world",
					"ping",
					"clients",
					"run-time",
				};
				String line = readLine();
				serverSays(line);
				
				outer:
				while (true) {
					for (String enquiry : enquiries) {
						writeOut(enquiry);
						if ( (line = readLine()) == null ) {
							break outer;
						}
						serverSays(line);
						sleep(10);
					}
				}
			}
		},
		new AbstractClient() {
			public void speak() throws IOException {
				String line = readLine();
				serverSays(line);
				while (line != null) {
					line = readLine();
					writeOut("clients");
					sleep(10);
				}
			}
		},
		new AbstractClient() {
			public void speak() throws IOException {
				String line = readLine();
				serverSays(line);
				writeOut("time");
				line = readLine();
				output.println(line + ": sleeping for 1 second...");
				sleep(1000);
				writeOut("time");
				line = readLine();
				serverSays(line);
				writeOut("goodbye");
				line = readLine();
				serverSays(line);
			}
		},
		new AbstractClient() {
			public void speak() throws IOException {
				String line = readLine();
				serverSays(line);
				sleep(3000);
				writeOut("shutdown");
				serverSays(readLine());
			}
		},
		new AbstractClient() {
			public void speak() throws IOException {
				socket.setSoTimeout(500);
				String line = readLine();
				serverSays(line);
				
				while (line != null) {
					try {
						writeOut("cat");
						line = readLine();
						serverSays(line);
						sleep(10);
					} catch (SocketTimeoutException e) {}
				}
			}
		},
		new AbstractClient() {
			public void speak() throws IOException {
				socket.setSoTimeout(500);
				String line = readLine();
				serverSays(line);
				
				while (line != null) {
					try {
						writeOut("dog");
						line = readLine();
						serverSays(line);
						sleep(10);
					} catch (SocketTimeoutException e) {}
				}
			}
		},
		new AbstractClient() {
			public void speak() throws IOException {
				socket.setSoTimeout(500);
				String line = readLine();
				serverSays(line);
				
				while (line != null) {
					try {
						writeOut("time");
						line = readLine();
						serverSays(line);
						sleep(10);
					} catch (SocketTimeoutException e) {}
				}
			}
		}
	};

	public static void main(String[] args) throws InterruptedException, IOException {
//		PrintStream newOut = new PrintStream(new FileOutputStream(new File("client_calls.log")));
//		System.setOut(newOut);
//		System.setErr(newOut);
		Map<String,String> settings = new HashMap<String,String>();
		settings.put(PlaygroundServer.ACTIVE_CONNECTIONS_KEY, "6");
		PlaygroundServer srv = new PlaygroundServer(settings);
		srv.debug = false;
		Map<String, BiConsumer<PlaygroundClient, String>> extraCommands = new HashMap<>();
		
		extraCommands.put("dog", (c,a) -> c.writeOut("woof"));
		extraCommands.put("cat", (c,a) -> c.writeOut("meow"));
		srv.putAllCommands(extraCommands);
		
		Thread srvThread = new Thread(srv);
		srvThread.start();
		
		int i = 0;
		List<Thread> clientThreads = new LinkedList<>();
		for (AbstractClient client : CLIENTS) {
			client.setHost("localhost");
			client.setPort(57001);
			client.setName("client" + i++);
			client.setOutput(new PrintWriter(new FileWriter(new File(client.getName() + ".txt"))));
			Thread thread = new Thread(client);
			thread.start();
			clientThreads.add(thread);
		}
		
		for (Thread thread : clientThreads) {
			thread.join();
		}
		srvThread.join();
	}
	
	static abstract class AbstractClient implements Runnable {
		Socket socket;
		private String host;
		private String name;
		private int port;
		private BufferedReader reader;
		private PrintWriter writer;
		PrintWriter output = new PrintWriter(System.out);
		
		public abstract void speak() throws IOException;
		
		public String getHost() {
			return host;
		}
		public void setHost(String host) {
			this.host = host;
		}
		public int getPort() {
			return port;
		}
		public void setPort(int port) {
			this.port = port;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public void setOutput(PrintWriter outputWriter) {
			output = outputWriter;
		}
		
		public void writeOut(String str) {
			sendingToServer(str);
			writer.println(str);
			writer.flush();
		}
		
		public String readLine() throws IOException {
			return reader.readLine();
		}
		
		protected void serverSays(String str) {
			output.println(getName() + ": server says '" + str + "'");
		}
		
		protected void sendingToServer(String str) {
			output.println(getName() + " sending to server: '" + str + "'");
		}
		
		protected void sleep(long millis) {
			try {
				Thread.sleep(millis);
			} catch (InterruptedException e) {}
		}
		
		public void run() {
			try (
				Socket socket = new Socket(getHost(), getPort());
				final BufferedReader reader = 
						new BufferedReader(
								new InputStreamReader(socket.getInputStream()));
				final PrintWriter writer = 
						new PrintWriter(
								new OutputStreamWriter(
										new BufferedOutputStream(
												socket.getOutputStream())))) {
				this.socket = socket;
				this.reader = reader;
				this.writer = writer;
				
				try {
					speak();
				} catch (SocketException e) {
					output.println("server's all done!");
				}
				output.flush();
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			}
		}
	}
}
