package edu.uweo.java2.homework.networking1;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;

public class PlaygroundServer implements Runnable {

	long startTime;
	boolean debug = false;
	boolean shutdown = false;
	AtomicInteger numActiveClients = new AtomicInteger(0);
	private final int numMaxClients;

	public static final String ACCEPT_TIMEOUT_KEY =			"accept-timeout";
	public static final String ACTIVE_CLIENTS_KEY = 		"active-clients";
	public static final String ACTIVE_CONNECTIONS_KEY =		"active-connections";
	public static final String CLIENT_TIMEOUT_KEY = 		"client-timeout";
	public static final String GREETING_KEY = 				"greeting";
	public static final String PORT_KEY = 					"port";
	
	public static final String ACCEPT_TIMEOUT_DEFAULT =		"500";
	public static final String ACTIVE_CLIENTS_DEFAULT =		"4";
	public static final String ACTIVE_CONNECTIONS_DEFAULT =	"8";
	public static final String CLIENT_TIMEOUT_DEFAULT =		"500";
	public static final String GREETING_DEFAULT =			"Welcome to the playground.";
	public static final String PORT_DEFAULT =				"57001";

	private final Map<String, String> settings = new HashMap<>();
	
	public static final Map<String,String> DEFAULT_SETTINGS;
	static {
		Map<String,String> map = new HashMap<>();
		map.put(ACCEPT_TIMEOUT_KEY, ACCEPT_TIMEOUT_DEFAULT);
		map.put(ACTIVE_CLIENTS_KEY, ACTIVE_CLIENTS_DEFAULT);
		map.put(ACTIVE_CONNECTIONS_KEY, ACTIVE_CONNECTIONS_DEFAULT);
		map.put(CLIENT_TIMEOUT_KEY, CLIENT_TIMEOUT_DEFAULT);
		map.put(GREETING_KEY, GREETING_DEFAULT);
		map.put(PORT_KEY, PORT_DEFAULT);
		DEFAULT_SETTINGS = Collections.unmodifiableMap(map);
	}

	private final Map<String,BiConsumer<PlaygroundClient,String>> commands = new HashMap<>();
	{
		commands.put("time", (c,a) -> c.writeOut(System.currentTimeMillis()));
		commands.put("echo", (c,a) -> c.writeOut(a));
		commands.put("ping", (c,a) -> c.writeOut("ack"));
		commands.put("clients", (c,a) -> c.writeOut(c.getServer().numActiveClients));
		commands.put("run-time", (c,a) -> c.writeOut(System.currentTimeMillis() - startTime));
		commands.put("goodbye", (c,a) -> { 
				c.writeOut("ack");
				Thread.currentThread().interrupt();
			});
		commands.put("shutdown", (c,a) -> {
				c.writeOut("ack");
				c.getServer().shutdown();
			});
	}
	
	private final BlockingQueue<Thread> managers;
	private final BlockingQueue<Client> activeClients;
	private final BlockingQueue<Client> pendingClients;
	
	public static void main(String... args) {
		Map<String,String> settings = new HashMap<>();
		settings.put(ACTIVE_CONNECTIONS_KEY, "4");
		
		PlaygroundServer srv = new PlaygroundServer(settings);
		srv.putCommand("add-response", (client,argString) -> {
			String[] tuple = argString.split("\\s", 2);
			if (tuple.length < 1) {
				return;
			}
			client.getServer().putCommand(tuple[0], (c,a) -> c.writeOut(tuple[1]));
			client.writeOut("command '" + tuple[0] + "' added");
		});
		srv.run();
	}

	// package-access constructor for debugging
	PlaygroundServer() {
		this(null);
	}

	public PlaygroundServer(Map<String, String> settings) {
		this.settings.putAll(DEFAULT_SETTINGS);
		if (settings != null) {
			this.settings.putAll(settings);
		}
		
		managers = new LinkedBlockingQueue<>(getInt(ACTIVE_CLIENTS_KEY));
		activeClients = 
				new LinkedBlockingQueue<>(getInt(ACTIVE_CONNECTIONS_KEY));
		pendingClients = new LinkedBlockingQueue<>();
		numMaxClients = getInt(ACTIVE_CONNECTIONS_KEY);
	}

	private String getString(String setting) {
		try {
			return settings.get(setting);
		} catch (NullPointerException e) {
			return DEFAULT_SETTINGS.get(setting);
		}
	}

	private int getInt(String setting) {
		try {
			return Integer.parseInt(settings.get(setting));
		} catch (NullPointerException e) {
			return Integer.parseInt(DEFAULT_SETTINGS.get(setting));
		}
	}
	
	protected void putAllCommands(Map<String,BiConsumer<PlaygroundClient,String>> extraCommands) {
		commands.putAll(extraCommands);
	}
	
	protected void putCommand(String name, BiConsumer<PlaygroundClient,String> command) {
		commands.put(name, command);
	}
	
	public BiConsumer<PlaygroundClient,String> removeCommand(String name) {
		switch (name) {
		case "echo":
		case "ping":
		case "run-time":
		case "time":
		case "clients":
		case "goodbye":
		case "shutdown":
			return null;
		default:
			return commands.remove(name);
		}
	}

	@Override
 	public void run() {
//		hello Marianne!
		startTime = System.currentTimeMillis();

		try (ServerSocket server = new ServerSocket(getInt(PORT_KEY))) {

//			 set server accept timeout
			server.setSoTimeout(getInt(CLIENT_TIMEOUT_KEY));

//			 get connections from clients
			Thread populateClients = new Thread(new PopulateClients(server));
			populateClients.start();
			
			Thread dequeuePendingClients = new Thread(new DequeuePendingClients());
			dequeuePendingClients.start();

//			 create threads to manage connections
			while (managers.remainingCapacity() > 0) {
				Thread t = new Thread(new ClientManager(), "manager "
						+ managers.remainingCapacity());
				t.start();
				debug("adding '" + t.getName() + "'");
				managers.offer(t);
			}

//			 wait for all threads to finish
			joinRemaining(populateClients, dequeuePendingClients);
		} catch (NumberFormatException | IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
		debug("all done with stuff");
	}
	
	private void joinRemaining(Thread... remainingLocals) {
		boolean doneJoining = false;
		while (!doneJoining) {
			try {
				for (Thread manager : managers) {
					manager.join();
					debug("joined on '" + manager.getName() + "'");
				}
				for (Thread remaining : remainingLocals) {
					remaining.join();
				}
				doneJoining = true;
			} catch (InterruptedException e) {}
		}
	}

	public void shutdown() {
		shutdown = true;
		for (Thread manager : managers) {
			manager.interrupt();
		}
	}

	void debug(String msg) {
		if (debug)
			System.err.println("DEBUG: " + msg);
	}

	void debug(Thread caller, String msg) {
		debug(String.format("'%s': %s", caller.getName(), msg));
	}

//	 inner classes

//	 handles new client management in a separate thread
	private class PopulateClients implements Runnable {
		private ServerSocket server;

		public PopulateClients(ServerSocket server) {
			this.server = server;
			Thread.currentThread().setName("PopulateClients");
		}

		public void run() {
			Socket sock = null;
			while (!shutdown) {
				try {
					sock = server.accept();
					Client client = new Client(sock);
					pendingClients.put(client);
				} catch (SocketTimeoutException | SocketException
						| InterruptedException e) {
				} catch (IOException e) {
					e.printStackTrace();
					System.exit(1);
				}
			}
		}
	}
	
//	dequeues pending connections
	private class DequeuePendingClients implements Runnable {
		public void run() {
			Thread.currentThread().setName("DequequePendingClients");
			while (!shutdown) {
				try {
					if ( numActiveClients.intValue() < numMaxClients) {
						Client client = pendingClients.poll(500, TimeUnit.MILLISECONDS);
						activeClients.put(client);
						numActiveClients.incrementAndGet();
					}
					Thread.sleep(50);
				} catch (InterruptedException|NullPointerException e) {}
			}
			
			while (!pendingClients.isEmpty()) {
				try {
					Client client = activeClients.take();
					client.handleShutdown();
				} catch (InterruptedException e) {
					Thread.interrupted();
				} catch (IOException e) {
					e.printStackTrace();
					System.exit(1);
				}
			}
		}
	}

//	 accesses and processes client requests
	private class ClientManager implements Runnable {
		public void run() {
			Thread currentThread = Thread.currentThread();

			while ( !( shutdown && activeClients.isEmpty() ) ) {
				Client client = null;
				try {
					debug(currentThread, "taking client");
					client = activeClients.take();

					debug(currentThread, "checking for shutdown");
					if (shutdown) {
						disconnectClient(client);
						continue;
					}
					
					try {
						debug(currentThread, "processing client request");
						client.process();
					} catch (SocketTimeoutException e) {}
					
					debug(currentThread, "putting client");
					activeClients.put(client);
					debug(currentThread,
							"client capacity is "
							+ activeClients.remainingCapacity());
				} catch (InterruptedException e) {
					debug(currentThread, "interrupted");
					disconnectClient(client);
				} catch (SocketException e) {
					if (client != null) {
						debug("client at '"
								+ client.socket.getInetAddress()
										.getHostAddress()
								+ "' has closed the connection");
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			debug(currentThread, "no clients to close; ending thread");
		}

		public void disconnectClient(Client client) {
			if (client != null) {
				try {
					client.handleShutdown();
					numActiveClients.decrementAndGet();
				} catch (IOException f) {
					f.printStackTrace();
					System.exit(1);
				}
			}
		}
	}

	// manages an individual client socket
	private class Client implements PlaygroundClient {
		Socket socket;
		PrintWriter writer;
		BufferedReader reader;
		PlaygroundServer server;
		
		public PlaygroundServer getServer() {
			return server;
		}

		Client(Socket soc) {
			socket = soc;
			try {
				socket.setSoTimeout(getInt(CLIENT_TIMEOUT_KEY));
				writer = new PrintWriter(new OutputStreamWriter(
						new BufferedOutputStream(socket.getOutputStream())));
				reader = new BufferedReader(new InputStreamReader(
						socket.getInputStream()));
				writeOut(getString(GREETING_KEY));
				server = PlaygroundServer.this;
			} catch (IOException | NumberFormatException e) {
				e.printStackTrace();
				System.exit(1);
			}
		}

		void process() throws IOException {
			String line = reader.readLine();
			debug(Thread.currentThread(), "client says '" + line + "'");
			execute(line);
		}

		void execute(String input) {
			String[] splitted = input.split("\\s", 2);
			String command = splitted[0];
			
			try {
				commands.get(command).accept(this, splitted.length > 1 ? splitted[1] : "");
			} catch (NullPointerException e) {
				writeOut("'" + command + "' is not a recognized command");
			}
		}

		public void writeOut(Object o) {
			debug(Thread.currentThread(), "responding: '" + o + "'");
			writer.println(o);
			writer.flush();
		}

		void handleShutdown() throws IOException {
			debug(Thread.currentThread(), "closing client");
			writeOut("SERVER SHUTTING DOWN");
			socket.close();
		}
	}
	
	public interface PlaygroundClient {
		public void writeOut(Object o);
		public PlaygroundServer getServer();
	}

}
